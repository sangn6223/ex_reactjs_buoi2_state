import React, { Component } from "react";

export default class Ex_car extends Component {
  state = { imgSrc: "./Img/CarBasic/products/red-car.jpg" };
  handleChangeColor = (color) => {
    let newImg = `./Img/CarBasic/products/${color}-car.jpg`;
    this.setState({ imgSrc: newImg });
  };
  render() {
    return (
      <div className="container row">
        <img className="col-8" src={this.state.imgSrc} />
        <div className="col-4 pt-5">
          <button
            onClick={() => {
              {
                this.handleChangeColor("red");
              }
            }}
            className="btn btn-danger"
          >
            Red
          </button>
          <button
            onClick={() => {
              this.handleChangeColor("black");
            }}
            className="btn btn-dark"
          >
            Black
          </button>
          <button
            onClick={() => {
              this.handleChangeColor("silver");
            }}
            className="btn btn-secondary"
          >
            Gray
          </button>
        </div>
      </div>
    );
  }
}
