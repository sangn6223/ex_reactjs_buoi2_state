import React, { Component } from "react";
import { movieArr } from "./DataMovie";

export default class RenderwithMap extends Component {
  state = {
    movieDetail: movieArr[0],
  };
  handleChangeDetail = (movie) => {
    this.setState({ movieDetail: movie });
  };
  renderMovieList = () => {
    let movieList = movieArr.map((item) => {
      return (
        <div className="card col-3 mt-3">
          <img
            style={{ height: "400px", objectFit: "cover" }}
            className="card-img-top "
            src={item.hinhAnh}
            alt="Card image cap"
          />
          <div className="card-body">
            <h5 className="card-title">{item.tenPhim}</h5>
            <p className="card-text">Mã Phim :{item.maPhim}</p>
            <a
              onClick={() => {
                this.handleChangeDetail(item);
              }}
              className="btn btn-primary"
            >
              Watch
            </a>
          </div>
        </div>
      );
    });

    return movieList;
  };
  render() {
    return (
      <div>
        <div className="display-4 text-danger">
          Name:{this.state.movieDetail.tenPhim}
        </div>
        <div className="container row mx-auto">{this.renderMovieList()}</div>
      </div>
    );
  }
}
