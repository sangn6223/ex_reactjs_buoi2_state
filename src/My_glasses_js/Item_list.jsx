import React, { Component } from "react";
import { glassesArr } from "./Data_glasses";

export default class Item_list extends Component {
  state = {
    imgSrc: "./glassesImage/v1.png",
    imgModel: "./glassesImage/model.jpg",
    noidung: "Brand",
    desc1: "Infor",
  };
  handleChangeType = (type, name, desc) => {
    let newImg = `${type}`;
    this.setState({ imgSrc: newImg, noidung: name, desc1: desc });
  };

  renderthongtinMangMatKinh = () => {
    let mang = glassesArr.map((sp) => {
      return (
        <div className="col-3">
          <button
            onClick={() => {
              this.handleChangeType(sp.url, sp.name, sp.desc);
            }}
          >
            <img src={sp.url} alt="" />
          </button>
        </div>
      );
    });
    return mang;
  };
  render() {
    return (
      <div>
        <div style={{ position: "relative" }} className="glass_model">
          <div
            style={{
              position: "absolute",
              top: "80px",
              width: "500px",
              height: "100px",
              backgroundColor: "red",
            }}
          >
            <img src={this.state.imgModel} alt="" />
            <img src={this.state.imgSrc} alt="" />
            Thông Tin : {this.state.noidung},{this.state.desc1}
          </div>
        </div>
        <div className="container row choice">
          {this.renderthongtinMangMatKinh()}
        </div>
      </div>
    );
  }
}
