import "./App.css";
import Item_list from "./My_glasses_js/Item_list";
import Model_glasses from "./My_glasses_js/Model_glasses";

function App() {
  return (
    <div className="App">
      <div className="banner">
        {/* <Model_glasses /> */}
        <Item_list />
      </div>
    </div>
  );
}

export default App;
